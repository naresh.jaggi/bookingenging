# -*- coding: utf-8 -*-

from django.db.models import (Q,
                              F,
                              Count,
                              Case,
                              DecimalField,
                              OuterRef,
                              Subquery,
                              When)
from django.db.models.functions import Cast
from decimal import Decimal
from rest_framework.viewsets import (ModelViewSet,
                                     ViewSet)
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from listings.models import (Listing,
                             HotelRoom,
                             BookingInfo,
                             ReservationInfo)
from api.serializers import ListingModelSerializer


class ListingsModelViewSet(ModelViewSet):

    serializer_class = ListingModelSerializer
    permision_classes = [IsAuthenticated,]
    http_method_names = ['get', 'head', 'options']

    def get_queryset(self):
        max_price = self.request.query_params.get('max_price')
        check_in = self.request.query_params.get('check_in')
        check_out = self.request.query_params.get('check_out')
        if max_price and check_in and check_out:
            filter_params = dict(reservation_infos__check_in__lte=check_out,
                                 reservation_infos__check_out__gte=check_in)
            hotel_listings = Listing.objects.filter(**filter_params,
                                                    reservation_infos__hotel_room__isnull=False)\
                                            .annotate(booked_rooms=Count('hotel_room_types__hotel_rooms__reservation_infos'),
                                                      total_rooms=Count('hotel_room_types__hotel_rooms')).filter(total_rooms=F('booked_rooms'))\
                                            .values_list('id', flat=True)
            listings = Listing.objects.exclude(Q(reservation_infos__hotel_room__isnull=True,
                                                 **filter_params) | Q(booking_info__price__gt=max_price))\
                                      .exclude(id__in=hotel_listings)
            listings = listings.annotate(price=Case(When(listing_type='apartment', then=Cast(F('booking_info__price'),
                                                                                              output_field=DecimalField())),
                                                    When(listing_type='hotel', then=Cast(Subquery(BookingInfo.objects.filter(price__lt=max_price,
                                                                                                                             hotel_room_type__hotel=OuterRef('pk')).values('price')),
                                                                                         output_field=DecimalField()))))
            return listings
        else:
            return Listing.objects.all()
