# -*- coding: utf-8 -*-

from django.urls import path
from rest_framework.routers import DefaultRouter
from api.views import ListingsModelViewSet

router = DefaultRouter()

router.register('units', ListingsModelViewSet, basename='units')

urlpatterns = []

urlpatterns += router.urls

