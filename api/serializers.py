# -*- coding: utf-8 -*-

from rest_framework.serializers import (ModelSerializer,
                                        DecimalField)
from listings.models import (Listing,)

class ListingModelSerializer(ModelSerializer):
    price = DecimalField(max_digits=6, decimal_places=2)
    
    class Meta:
        model = Listing
        fields = '__all__'
