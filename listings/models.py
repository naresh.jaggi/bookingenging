# -*- coding: utf-8 -*-

from django.db import models
from django.core.exceptions import ValidationError

class Listing(models.Model):
    HOTEL = 'hotel'
    APARTMENT = 'apartment'
    LISTING_TYPE_CHOICES = (
        ('hotel', 'Hotel'),
        ('apartment', 'Apartment'),
    )

    listing_type = models.CharField(
        max_length=16,
        choices=LISTING_TYPE_CHOICES,
        default=APARTMENT
    )
    title = models.CharField(max_length=255,)
    country = models.CharField(max_length=255,)
    city = models.CharField(max_length=255,)

    def __str__(self):
        return self.title
    

class HotelRoomType(models.Model):
    hotel = models.ForeignKey(
        Listing,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name='hotel_room_types'
    )
    title = models.CharField(max_length=255,)

    def __str__(self):
        return f'{self.hotel} - {self.title}'


class HotelRoom(models.Model):
    hotel_room_type = models.ForeignKey(
        HotelRoomType,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name='hotel_rooms'
    )
    room_number = models.CharField(max_length=255,)

    def __str__(self):
        return f'{self.hotel_room_type} {self.room_number}'


class BookingInfo(models.Model):
    listing = models.OneToOneField(
        Listing,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='booking_info'
    )
    hotel_room_type = models.OneToOneField(
        HotelRoomType,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        related_name='booking_info',
    )
    price = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        if self.listing:
            obj = self.listing
        else:
            obj = self.hotel_room_type
            
        return f'{obj} {self.price}'


class ReservationInfo(models.Model):
    check_in = models.DateField(
        help_text='start date for reservation'
    )
    check_out = models.DateField(
        help_text='end date for reservation'
    )
    listing = models.ForeignKey(
        Listing,
        on_delete=models.CASCADE,
        related_name='reservation_infos',
        help_text='reserved type listings')
    hotel_room = models.ForeignKey(
        HotelRoom,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name='reservation_infos',
        help_text='reserved hotel rooms'
    )

    def __str__(self):
        if self.hotel_room:
            return f'{self.listing} | {self.hotel_room} | {self.check_in} - {self.check_out}'
        else:
            return f'{self.listing} | {self.check_in} - {self.check_out}'

    def clean(self):
        super(ReservationInfo, self).clean()

        if self.listing.listing_type == 'apartment' and self.hotel_room:
            raise ValidationError({'hotel_room': 'Reservations have already been made for this date range.'})
        else:
            pass
        if self.hotel_room:
            if ReservationInfo.objects.filter(check_in__lte=self.check_out,
                                              check_out__gte=self.check_in,
                                              hotel_room=self.hotel_room).exists():
                raise ValidationError({'hotel_room': 'Reservations have already been made for this date range.'})
            elif self.hotel_room.hotel_room_type.hotel != self.listing:
                raise ValidationError({'hotel_room': 'Hotel room does not belong to select listing'})
            else:
                pass
        else:
            if ReservationInfo.objects.filter(check_in__lte=self.check_out,
                                              check_out__gte=self.check_in,
                                              listing=self.listing).exists():
                raise ValidationError({'listing': 'Reservations have already been made for this date range.'})
            else:
                pass
        if self.check_in > self.check_out:
            raise ValidationError({'check_in': 'Check in date should be larger than checkout date.'})
        else:
            pass
                            
    class Meta:
        verbose_name = 'Reservation Information'
        verbose_name_plural = 'Reservation Informations'
